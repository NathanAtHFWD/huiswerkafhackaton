package nl.headfwd.huiswerk.af.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = false)
public class Step extends ResourceSupport {

    @Id
    @JsonIgnore
    public String identification;
    
    @JsonProperty
    private final int step;
    
    @JsonProperty
    private final String answer;

    @JsonProperty
    private final String methodIdentification;
}
