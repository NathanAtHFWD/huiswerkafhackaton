package nl.headfwd.huiswerk.af.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@EqualsAndHashCode(callSuper = false)
@Relation(value = "course", collectionRelation = "courses")
public class Course extends ResourceSupport {

    @Id
    @JsonIgnore
    public String identification;

    @JsonProperty
    private final String courseName;
    @JsonProperty
    private final String educationLevel;
    @JsonProperty
    private final int year;
    @JsonProperty
    private final int difficulty;
}
