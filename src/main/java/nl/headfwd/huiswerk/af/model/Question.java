package nl.headfwd.huiswerk.af.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.hateoas.ResourceSupport;

@Data
@EqualsAndHashCode(callSuper = false)
public class Question extends ResourceSupport {

    @Id
    @JsonIgnore
    public String identification;

    @JsonProperty
    private final String name;
    @JsonProperty
    private final String question;

    @JsonProperty
    private final String courseIdentification;
}
