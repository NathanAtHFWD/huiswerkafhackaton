package nl.headfwd.huiswerk.af.repos;

import nl.headfwd.huiswerk.af.model.Course;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends MongoRepository<Course, String> {

    List<Course> findAllByEducationLevelEquals(String educationLevel);

    List<Course> findAllByDifficulty(int difficulty);

    List<Course> findCoursesByEducationLevelAndYear(String educationLevel, int year);

    Course findCourseByIdentification(String courseId);

    List<Course> findCoursesByDifficultyGreaterThanEqual(int difficulty);


}
