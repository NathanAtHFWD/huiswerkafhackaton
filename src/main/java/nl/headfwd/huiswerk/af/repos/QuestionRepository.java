package nl.headfwd.huiswerk.af.repos;

import nl.headfwd.huiswerk.af.model.Question;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends MongoRepository<Question, String> {

    List<Question> findAllQuestionsByCourseIdentification(String courseIdentification);

    Question findQuestionByIdentification(String questionId);
}
