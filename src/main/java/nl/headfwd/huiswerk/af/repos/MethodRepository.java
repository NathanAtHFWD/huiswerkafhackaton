package nl.headfwd.huiswerk.af.repos;

import nl.headfwd.huiswerk.af.model.Method;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MethodRepository extends MongoRepository<Method, String> {

    List<Method> findMethodsByQuestionIdentification(String questionIdentification);

    Method findMethodByIdentification(String methodId);
}
