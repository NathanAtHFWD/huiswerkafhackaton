package nl.headfwd.huiswerk.af.repos;

import nl.headfwd.huiswerk.af.model.Step;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StepRepository extends MongoRepository<Step, String>{

    Step findFirstByAnswerEquals(String answer);

    List<Step> findStepsByMethodIdentification(String methodIdentification);

    Step findStepByIdentification(String stepId);
}
