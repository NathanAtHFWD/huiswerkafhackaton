package nl.headfwd.huiswerk.af.repos;

import nl.headfwd.huiswerk.af.model.Hint;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HintRepository extends MongoRepository<Hint, String> {

    List<Hint> findAllHintsByStepIdentification(String stepIdentification);

    Hint findCourseByIdentification(String hintId);
}
