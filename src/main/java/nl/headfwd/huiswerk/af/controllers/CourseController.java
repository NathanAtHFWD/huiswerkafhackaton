package nl.headfwd.huiswerk.af.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.headfwd.huiswerk.af.model.Course;
import nl.headfwd.huiswerk.af.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static nl.headfwd.huiswerk.af.util.serializer.serializeEmbedded;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class CourseController {

    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping(path = "/courses", produces = {"application/hal+json"})
    public ResponseEntity<String> getCoursesByEducationLevelAndYear(
            @RequestParam(value = "educationLevel", required = false) String educationLevel,
            @RequestParam(value = "year", required = false) Integer year) throws JsonProcessingException {
        List<Course> courses = new ArrayList<>();
        if (educationLevel == null && year == null) {
            courses = courseService.getAllCourses();
        }
        if (educationLevel != null && year != null) {
            courses = courseService.getCoursesByEducationLevelAndYear(educationLevel, year);
        }
        if (courses.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        Resources<Course> resources = populateWithLinks(courses, educationLevel, year);

        return ResponseEntity.ok().body(serializeEmbedded(resources));
    }

    @GetMapping(path="/courses/{cid}", produces = {"application/hal+json"})
    public ResponseEntity<Course> getResourceById(@PathVariable("cid") String id){
        return ResponseEntity.ok().body(courseService.findCourseByIdentification(id));
    }

    private Resources<Course> populateWithLinks(List<Course> courses, String educationLevel, Integer year) throws JsonProcessingException {
        List<Course> resources = new ArrayList<>(courses.size());
        for (Course course : courses) {

            Link selflink = linkTo(CourseController.class).slash("courses").slash(course.identification).withSelfRel();

            // Add questions to the given course
            Link questions = linkTo(CourseController.class).slash("courses").slash(course.identification)
                    .slash("questions").withRel("questions");

            course.add(selflink, questions);
            resources.add(course);
        }
        return new Resources<>(resources,  linkTo(methodOn(getClass()).getCoursesByEducationLevelAndYear(educationLevel, year)).withSelfRel());
    }

}
