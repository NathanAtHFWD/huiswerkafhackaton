package nl.headfwd.huiswerk.af.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.headfwd.huiswerk.af.model.Method;
import nl.headfwd.huiswerk.af.services.MethodService;
import nl.headfwd.huiswerk.af.util.serializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class MethodController {
    private final MethodService methodService;

    @Autowired
    public MethodController(MethodService methodService) {
        this.methodService = methodService;
    }

    @GetMapping(path = "/courses/{cid}/questions/{qid}/methods")
    public ResponseEntity<String> getMethodsForQuestion(@PathVariable("cid") String courseIdentification,
                                                        @PathVariable("qid") String questionIdentification) throws JsonProcessingException {

        List<Method> methods = methodService.getMethodsForQuestion(questionIdentification);

        Resources<Method> resources = populateWithLinks(courseIdentification, questionIdentification, methods);
        return ResponseEntity.ok().body(serializer.serializeEmbedded(resources));
    }

    @GetMapping(path="/methods/{cid}", produces = {"application/hal+json"})
    public ResponseEntity<Method> getResourceById(@PathVariable("cid") String id){
        return ResponseEntity.ok().body(methodService.findMethodByIdentification(id));
    }

    private Resources<Method> populateWithLinks(String courseIdentification, String questionIdentification, List<Method> methods) throws JsonProcessingException {
        List<Method> resources = new ArrayList<>(methods.size());
        for (Method method : methods) {
            Link selfLink = linkTo(methodOn(MethodController.class).getMethodsForQuestion(courseIdentification, questionIdentification))
                    .slash(method.identification).withSelfRel();
            method.add(selfLink);

            // Add steps to the given course
            Link steps = linkTo(methodOn(MethodController.class).getMethodsForQuestion(courseIdentification, questionIdentification))
                    .slash(method.identification)
                    .slash("steps").withRel("steps");

            method.add(steps);
            resources.add(method);
        }
        Link selfLink = linkTo(methodOn(getClass()).getMethodsForQuestion(courseIdentification, questionIdentification)).withSelfRel();
        return new Resources<>(resources, selfLink);
    }
}
