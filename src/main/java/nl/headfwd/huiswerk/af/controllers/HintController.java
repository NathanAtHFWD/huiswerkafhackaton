package nl.headfwd.huiswerk.af.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.headfwd.huiswerk.af.model.Hint;
import nl.headfwd.huiswerk.af.services.HintsService;
import nl.headfwd.huiswerk.af.util.serializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class HintController {

    private HintsService hintsService;

    @Autowired
    public HintController(HintsService hintsService) {
        this.hintsService = hintsService;
    }


    @GetMapping(path = "/courses/{cid}/questions/{qid}/methods/{mid}/steps/{sid}/hints")
    public ResponseEntity<String> getHintsForStep(@PathVariable("cid") String courseIdentification,
                                                  @PathVariable("qid") String questionIdentification,
                                                  @PathVariable("mid") String methodIdentification,
                                                  @PathVariable("sid") String stepIdentification) throws JsonProcessingException {
        List<Hint> hints = hintsService.getHintsForStep(stepIdentification);

        Resources<Hint> resources = populateWithLinks(courseIdentification, questionIdentification, methodIdentification, stepIdentification, hints);

        return ResponseEntity.ok().body(serializer.serializeEmbedded(resources));    }

    @GetMapping(path="/hints/{cid}", produces = {"application/hal+json"})
    public ResponseEntity<Hint> getResourceById(@PathVariable("cid") String id){
        return ResponseEntity.ok().body(hintsService.findHintByIdentification(id));
    }


    private Resources<Hint> populateWithLinks(String courseIdentification, String questionIdentification, String methodIdentification, String stepIdentification, List<Hint> hints) throws JsonProcessingException {
        List<Hint> resources = new ArrayList<>(hints.size());
        for (Hint hint : hints) {
            Link selfLink = linkTo(methodOn(HintController.class).getHintsForStep(courseIdentification, questionIdentification, methodIdentification, stepIdentification))
                    .slash(hint.identification).withSelfRel();
            hint.add(selfLink);
            resources.add(hint);
        }
        Link selfLink = linkTo(methodOn(getClass()).getHintsForStep(courseIdentification, questionIdentification, methodIdentification, stepIdentification)).withSelfRel();
        return new Resources<>(resources, selfLink);
    }
}
