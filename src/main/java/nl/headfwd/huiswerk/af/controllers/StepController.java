package nl.headfwd.huiswerk.af.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.headfwd.huiswerk.af.model.Step;
import nl.headfwd.huiswerk.af.services.StepService;
import nl.headfwd.huiswerk.af.util.serializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class StepController {

    private final StepService stepService;

    @Autowired
    public StepController(StepService stepService) {
        this.stepService = stepService;
    }

    @GetMapping(path = "/courses/{cid}/questions/{qid}/methods/{mid}/steps")
    public ResponseEntity<String> getStepsForMethod(@PathVariable("cid") String courseIdentification,
                                                    @PathVariable("qid") String questionIdentification,
                                                    @PathVariable("mid") String methodIdentification) throws JsonProcessingException {
        List<Step> steps = stepService.getStepsForMethod(methodIdentification);

        Resources<Step> resources = populateWithLinks(courseIdentification, questionIdentification, methodIdentification, steps);
        return ResponseEntity.ok().body(serializer.serializeEmbedded(resources));
    }

    @GetMapping(path="/steps/{cid}", produces = {"application/hal+json"})
    public ResponseEntity<Step> getResourceById(@PathVariable("cid") String id){
        return ResponseEntity.ok().body(stepService.findStepByIdentification(id));
    }

    private Resources<Step> populateWithLinks(String courseIdentification, String questionIdentification, String methodIdentification, List<Step> steps) throws JsonProcessingException {
        List<Step> resources = new ArrayList<>(steps.size());
        for (Step step : steps) {
            Link selfLink = linkTo(methodOn(StepController.class).getStepsForMethod(courseIdentification, questionIdentification, methodIdentification))
                    .slash(step.identification).withSelfRel();
            step.add(selfLink);

            // Add hints to the given course
            Link hints = linkTo(methodOn(StepController.class).getStepsForMethod(courseIdentification, questionIdentification, methodIdentification))
                    .slash(step.identification)
                    .slash("hints").withRel("hints");

            step.add(hints);
            resources.add(step);
        }
        Link selfLink = linkTo(methodOn(getClass()).getStepsForMethod(courseIdentification, questionIdentification, methodIdentification)).withSelfRel();
        return new Resources<>(resources, selfLink);
    }

}
