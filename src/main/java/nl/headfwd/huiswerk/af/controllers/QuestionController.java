package nl.headfwd.huiswerk.af.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.headfwd.huiswerk.af.model.Question;
import nl.headfwd.huiswerk.af.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static nl.headfwd.huiswerk.af.util.serializer.serializeEmbedded;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class QuestionController {

    private final QuestionService service;

    @Autowired
    public QuestionController(QuestionService service) {

        this.service = service;
    }

    @GetMapping(path = "/courses/{id}/questions")
    public ResponseEntity<String> getQuestionsForCourse(@PathVariable("id") String courseIdentification) throws JsonProcessingException {
        List<Question> questions = service.getQuestionsByCourseIdentification(courseIdentification);

        Resources<Question> resources = populateWithLinks(courseIdentification, questions);

        return ResponseEntity.ok(serializeEmbedded(resources));
    }

    @GetMapping(path="/questions/{cid}", produces = {"application/hal+json"})
    public ResponseEntity<Question> getResourceById(@PathVariable("cid") String id){
        return ResponseEntity.ok().body(service.findQuestionByIdentification(id));
    }

    private Resources<Question> populateWithLinks(String courseIdentification, List<Question> questions) throws JsonProcessingException {
        List<Question> resources = new ArrayList<>(questions.size());
        for (Question question : questions) {
            Link selflink = linkTo(methodOn(QuestionController.class).getQuestionsForCourse(courseIdentification))
                    .slash(question.identification).withSelfRel();
            question.add(selflink);

            // Add methods to the given course
            Link methods = linkTo(methodOn(QuestionController.class).getQuestionsForCourse(courseIdentification))
                    .slash(question.identification)
                    .slash("methods").withRel("methods");

            question.add(methods);
            resources.add(question);
        }
        return new Resources<>(resources, linkTo(methodOn(getClass()).getQuestionsForCourse(courseIdentification)).withSelfRel());
    }
}
