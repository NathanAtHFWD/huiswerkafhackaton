package nl.headfwd.huiswerk.af.services;

import nl.headfwd.huiswerk.af.model.Course;
import nl.headfwd.huiswerk.af.repos.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CourseService {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> getCoursesByEducationLevelAndYear(String educationLevel, int year) {
        return courseRepository.findCoursesByEducationLevelAndYear(educationLevel.toUpperCase(), year);
    }

    public Course findCourseByIdentification(String courseId) {
        return courseRepository.findCourseByIdentification(courseId);
    }

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }
}
