package nl.headfwd.huiswerk.af.services;

import nl.headfwd.huiswerk.af.model.Method;
import nl.headfwd.huiswerk.af.repos.MethodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MethodService {

    private final MethodRepository methodRepository;

    @Autowired
    public MethodService(MethodRepository methodRepository) {
        this.methodRepository = methodRepository;
    }

    public List<Method> getMethodsForQuestion(String questionIdentification) {
        return methodRepository.findMethodsByQuestionIdentification(questionIdentification);
    }

    public Method findMethodByIdentification(String methodId) {
        return methodRepository.findMethodByIdentification(methodId);
    }
}
