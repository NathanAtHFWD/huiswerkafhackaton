package nl.headfwd.huiswerk.af.services;

import nl.headfwd.huiswerk.af.model.*;
import nl.headfwd.huiswerk.af.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataService {

    private final CourseRepository courseRepository;
    private final QuestionRepository questionRepository;
    private final MethodRepository methodRepository;
    private final StepRepository stepRepository;
    private final HintRepository hintRepository;

    @Autowired
    public DataService(CourseRepository courseRepository,
                       QuestionRepository questionRepository,
                       MethodRepository methodRepository,
                       StepRepository stepRepository,
                       HintRepository hintRepository) {
        this.courseRepository   = courseRepository;
        this.questionRepository = questionRepository;
        this.methodRepository   = methodRepository;
        this.stepRepository     = stepRepository;
        this.hintRepository     = hintRepository;
        init();
    }
    private void init() {

        courseRepository.deleteAll();
        questionRepository.deleteAll();
        methodRepository.deleteAll();
        stepRepository.deleteAll();
        hintRepository.deleteAll();

        Course vwo3B1 = new Course("wis b L1", "VWO", 3, 1);
        Course vwo3B2 = new Course("wis b L2", "VWO", 3, 2);
        Course vwo3B3 = new Course("wis b L3", "VWO", 4, 3);
        Course havo3A1 = new Course("wis a L1", "HAVO", 3, 1);
        Course havo3A2 = new Course("wis a L2", "HAVO", 3, 2);

        courseRepository.save(vwo3B1);
        courseRepository.save(vwo3B2);
        courseRepository.save(vwo3B3);
        courseRepository.save(havo3A1);
        courseRepository.save(havo3A2);

        Question q1 = new Question("name", "Bereken maar iets", havo3A1.identification);
        questionRepository.save(q1);

        Question q2 = new Question("name", "Bereken maar iets", vwo3B1.identification);
        questionRepository.save(q2);

        Question q3 = new Question("name", "Bereken maar iets", vwo3B3.identification);
        questionRepository.save(q3);

        Question q4 = new Question("name", "Bereken maar iets11", havo3A1.identification);
        questionRepository.save(q4);

        Method m1 = new Method("method1", q1.identification);
        methodRepository.save(m1);

        Method m2 = new Method("method2", q2.identification);
        methodRepository.save(m2);

        Method m3 = new Method("method3", q3.identification);
        methodRepository.save(m3);

        Method m4 = new Method("method35", q4.identification);
        methodRepository.save(m4);

        Method m5 = new Method("method34", q4.identification);
        methodRepository.save(m5);

        Step s1 = new Step(1, " stapje 1", m1.identification);
        stepRepository.save(s1);

        Step s2 = new Step(2, " stapje 2", m2.identification);
        stepRepository.save(s2);

        Step s3 = new Step(2, " stapje 2", m3.identification);
        stepRepository.save(s3);

        Hint h1 = new Hint("thijs", "Thijs geeft een goede hint", s1.identification);
        hintRepository.save(h1);

        Hint h2 = new Hint("bob", "Bob geeft een goede hint", s2.identification);
        hintRepository.save(h2);

        Hint h3 = new Hint("niemand", "Niemand geeft een goede hint", s3.identification);
        hintRepository.save(h3);
    }

}
