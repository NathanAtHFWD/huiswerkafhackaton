package nl.headfwd.huiswerk.af.services;

import nl.headfwd.huiswerk.af.model.Step;
import nl.headfwd.huiswerk.af.repos.StepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StepService {

    private StepRepository stepRepository;

    @Autowired
    public StepService(StepRepository stepRepository) {
        this.stepRepository = stepRepository;
    }

    public List<Step> getStepsForMethod(String methodIdentification){
        return stepRepository.findStepsByMethodIdentification(methodIdentification);
    }

    public Step findStepByIdentification(String stepId) {
        return stepRepository.findStepByIdentification(stepId);
    }
}