package nl.headfwd.huiswerk.af.services;

import nl.headfwd.huiswerk.af.model.Question;
import nl.headfwd.huiswerk.af.repos.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository){
        this.questionRepository = questionRepository;
    }

    public List<Question> getQuestionsByCourseIdentification(String courseIdentification){
        return questionRepository.findAllQuestionsByCourseIdentification(courseIdentification);
    }

    public Question findQuestionByIdentification(String questionId) {
        return questionRepository.findQuestionByIdentification(questionId);
    }

}
