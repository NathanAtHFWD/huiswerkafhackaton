package nl.headfwd.huiswerk.af.services;

import nl.headfwd.huiswerk.af.model.Hint;
import nl.headfwd.huiswerk.af.repos.HintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HintsService {

    private final HintRepository hintRepository;

    @Autowired
    public HintsService(HintRepository hintRepository) {
        this.hintRepository = hintRepository;
    }

    public List<Hint> getHintsForStep(String stepIdentification) {
        return hintRepository.findAllHintsByStepIdentification(stepIdentification);
    }

    public Hint findHintByIdentification(String hintId) {
        return hintRepository.findCourseByIdentification(hintId);
    }
}
