${Ansi.RED}  _   _           _                                  _     ${Ansi.BLUE}    ${Ansi.GREEN}          __
${Ansi.RED} | | | |  _   _  (_)  ___  __      __   ___   _ __  | | __ ${Ansi.BLUE}    ${Ansi.GREEN}  __ _   / _|
${Ansi.RED} | |_| | | | | | | | / __| \ \ /\ / /  / _ \ | '__| | |/ / ${Ansi.BLUE}    ${Ansi.GREEN} / _` | | |_
${Ansi.RED} |  _  | | |_| | | | \__ \  \ V  V /  |  __/ | |    |   <  ${Ansi.BLUE} _  ${Ansi.GREEN}| (_| | |  _|
${Ansi.RED} |_| |_|  \__,_| |_| |___/   \_/\_/    \___| |_|    |_|\_\ ${Ansi.BLUE}(_) ${Ansi.GREEN} \__,_| |_|

${Ansi.DEFAULT} :: Spring Boot${spring-boot.formatted-version} :: ${Ansi.DEFAULT}
